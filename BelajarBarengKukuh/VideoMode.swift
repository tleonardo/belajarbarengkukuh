//
//  VideoMode.swift
//  BelajarBarengKukuh
//
//  Created by Timotius Leonardo Lianoto on 28/04/20.
//  Copyright © 2020 Timotius Leonardo Lianoto. All rights reserved.
//

import Foundation
/**
 Ini class VideoMode
 */
class VideoMode {
    var resolution = Resolution()
       var interlaced = false
       var frameRate = 0.0
       var name: String?
}
