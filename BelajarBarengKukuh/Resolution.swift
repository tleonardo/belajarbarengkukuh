//
//  Resolution.swift
//  BelajarBarengKukuh
//
//  Created by Timotius Leonardo Lianoto on 28/04/20.
//  Copyright © 2020 Timotius Leonardo Lianoto. All rights reserved.
//

import Foundation

struct Resolution {
    var height = 0
    var weight = 0
}
